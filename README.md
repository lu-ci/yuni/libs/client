# Client

Simply a wrapper structure to combine Serenity's client and handler with other details.
Such as the database, cache, configuration, intermediate storage, module management, etc.